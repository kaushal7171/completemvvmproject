package com.example.completemvvmdemo.Repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.completemvvmdemo.Database.DataBaseBooks;
import com.example.completemvvmdemo.InterFace.BookDao;
import com.example.completemvvmdemo.InterFace.CategoryDao;
import com.example.completemvvmdemo.model.Book;
import com.example.completemvvmdemo.model.Category;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BookRepositry {
    private CategoryDao mCategoryDao;
    private BookDao mBookDao;
    private LiveData<List<Category>> categories;
    private LiveData<List<Book>> books;

    public BookRepositry(Application application){
        DataBaseBooks dataBaseBooks=DataBaseBooks.getDataBaseInstance(application);
        mCategoryDao=dataBaseBooks.getCategoryDao();
        mBookDao=dataBaseBooks.getBookDao();

    }

    public LiveData<List<Category>> getCategories() {
        return mCategoryDao.getAllCategory();

    }

    public LiveData<List<Book>> getBooks(int category_id) {
        return mBookDao.getBooks(category_id);
    }


    public void insertCategory(Category category){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mCategoryDao.insertCategory(category);
            }
        });
    }
    public void insertBook(Book book){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mBookDao.insertBook(book);
            }
        });
    }

    public  void updateCtegory(Category category){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mCategoryDao.updateCategory(category);
            }
        });
    }
    public  void updateBook(Book book){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mBookDao.updateBook(book);
            }
        });
    }
    public  void deleteCategory(Category category){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
               mCategoryDao.deleteCategory(category);
            }
        });
    }

    public  void deleteBook(Book book){
        Executor executor= Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mBookDao.deleteBook(book);
            }
        });
    }


}

