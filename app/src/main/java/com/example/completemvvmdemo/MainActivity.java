package com.example.completemvvmdemo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.completemvvmdemo.InterFace.BookItemClickListener;
import com.example.completemvvmdemo.model.Book;
import com.example.completemvvmdemo.model.Category;
import com.example.completemvvmdemo.ViewModel.MainActivityViewModel;
import com.example.completemvvmdemo.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public MainActivityViewModel mainActivityViewModel;
    private ActivityMainBinding activityMainBinding;
    public Category selectedCategory;
    private ArrayList<Category> categoryList;
    private ArrayList<Book> bookArrayList;
    private RecyclerView rv_books;

    public static final int ADD_BOOK_REQUEST_CODE=1;
    public static final int EDIT_BOOK_REQUEST_CODE=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mainActivityViewModel=new ViewModelProvider(this).get(MainActivityViewModel.class);
        mainActivityViewModel= ViewModelProviders.of(this).get(MainActivityViewModel.class);
        activityMainBinding= DataBindingUtil.setContentView(MainActivity.this,R.layout.activity_main);
        activityMainBinding.setClickHandler(new ClickHandlers());

        mainActivityViewModel.getCategories().observe(this, new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                categoryList= (ArrayList<Category>) categories;
                showSpinnerData();
            }
        });


    }

    private void showSpinnerData() {
        ArrayAdapter<Category> categoryArrayAdapter=new ArrayAdapter<Category>(this,R.layout.spinner_item,categoryList);
        categoryArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        activityMainBinding.setSpinnerAdapter(categoryArrayAdapter);
    }

    public class ClickHandlers {
        public void setAddButtonClick() {
            Intent intent=new Intent(MainActivity.this,EditActivity.class);
            startActivityForResult(intent,ADD_BOOK_REQUEST_CODE);
        }

        public void onSelectItem(AdapterView<?> parent, View view, int pos, long id) {

            selectedCategory = (Category) parent.getItemAtPosition(pos);

            String message = " id is " + selectedCategory.getId() + "\n name is " + selectedCategory.getName() + "\n email is " + selectedCategory.getDescription();

            loadBookList(selectedCategory.getId());
            // Showing selected spinner item
            Toast.makeText(parent.getContext(), message, Toast.LENGTH_LONG).show();

        }
    }
    public void loadBookList(int selectedCategory){

        mainActivityViewModel.getBooks(selectedCategory).observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(List<Book> books) {
                bookArrayList= (ArrayList<Book>) books;
                loadRecyclerView();
            }
        });

    }
    private void loadRecyclerView(){
        rv_books=activityMainBinding.secondaryLayout.rvBooks;
        rv_books.setLayoutManager(new LinearLayoutManager(MainActivity.this,RecyclerView.VERTICAL,false));
        BookAdapter bookAdapter=new BookAdapter();
        rv_books.setAdapter(bookAdapter);
        bookAdapter.setBooksList(bookArrayList);

        bookAdapter.setBookItemClickListener(new BookItemClickListener() {
            @Override
            public void itemClick(Book book) {
                Intent intent=new Intent(MainActivity.this,EditActivity.class);
                intent.putExtra("bookObj",book);
                startActivityForResult(intent,EDIT_BOOK_REQUEST_CODE);
            }
        });
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                mainActivityViewModel.deleteBook(bookArrayList.get(viewHolder.getAdapterPosition()));
            }
        }).attachToRecyclerView(rv_books);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==ADD_BOOK_REQUEST_CODE && resultCode==RESULT_OK){
            Book book= (Book) data.getSerializableExtra("EditBook");
            book.setCategoryId(selectedCategory.getId());
            mainActivityViewModel.addBooks(book);

        }else if(requestCode==EDIT_BOOK_REQUEST_CODE && resultCode==RESULT_OK){
            Book book= (Book) data.getSerializableExtra("EditBook");
            mainActivityViewModel.updateBook(book);

        }
    }
}