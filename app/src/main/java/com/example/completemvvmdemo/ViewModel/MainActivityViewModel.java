package com.example.completemvvmdemo.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.completemvvmdemo.model.Book;
import com.example.completemvvmdemo.model.Category;
import com.example.completemvvmdemo.Repository.BookRepositry;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

     BookRepositry mBookRepositry;
    private LiveData<List<Category>> categories;
    private LiveData<List<Book>> books;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        mBookRepositry=new BookRepositry(application);
    }

    public LiveData<List<Category>> getCategories() {
        categories=mBookRepositry.getCategories();
        return categories;
    }

    public LiveData<List<Book>> getBooks(int selectedCategory) {
        books=mBookRepositry.getBooks(selectedCategory);
        return books;
    }
    public void addBooks(Book book){
        mBookRepositry.insertBook(book);
    }
    public void updateBook(Book book){
        mBookRepositry.updateBook(book);
    }
    public void deleteBook(Book book){
        mBookRepositry.deleteBook(book);
    }
}
