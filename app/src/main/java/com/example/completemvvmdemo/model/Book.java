package com.example.completemvvmdemo.model;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.completemvvmdemo.BR;

import java.io.Serializable;
import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "Book_table",foreignKeys = @ForeignKey(entity = Category.class,parentColumns = "id",
childColumns ="Category_Id",onDelete = CASCADE))
public class Book extends BaseObservable implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int bookId;

    @ColumnInfo(name = "Book_Name")
    private String bookName;

    @ColumnInfo(name = "Unit_Price")
    private String unitPrice;

    @ColumnInfo(name="Category_Id")
    private int categoryId;

    @Ignore
    public Book(){

    }


    public Book(int bookId, String bookName, String unitPrice, int categoryId) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.unitPrice = unitPrice;
        this.categoryId = categoryId;
    }

    @Bindable
    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
        notifyPropertyChanged(BR.bookId);
    }
    @Bindable
    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
        notifyPropertyChanged(BR.bookName);
    }
    @Bindable
    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
        notifyPropertyChanged(BR.unitPrice);
    }
    @Bindable
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        notifyPropertyChanged(BR.categoryId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return getBookId() == book.getBookId() &&
                getCategoryId() == book.getCategoryId() &&
                getBookName().equals(book.getBookName()) &&
                getUnitPrice().equals(book.getUnitPrice());
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(getBookId(), getBookName(), getUnitPrice(), getCategoryId());
    }
}
