package com.example.completemvvmdemo.Database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.completemvvmdemo.InterFace.BookDao;
import com.example.completemvvmdemo.InterFace.CategoryDao;
import com.example.completemvvmdemo.model.Book;
import com.example.completemvvmdemo.model.Category;

@Database(entities = {Category.class, Book.class},version = 1)
public abstract class DataBaseBooks extends RoomDatabase {

    public static DataBaseBooks instance;
    public abstract CategoryDao getCategoryDao();
    public  abstract BookDao getBookDao();

    public static synchronized DataBaseBooks  getDataBaseInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context,DataBaseBooks.class,"BookDb.db")
                    .fallbackToDestructiveMigration()
                    .addCallback(callback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback callback=new RoomDatabase.Callback(){

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new InsertInitialData(instance).execute();
            Log.d("Tag","onCreate Database call");
        }
    };
    private static class InsertInitialData extends AsyncTask<Void , Void ,Void>{

        private CategoryDao mCategoryDao;
        private BookDao mBookDao;

        private InsertInitialData(DataBaseBooks dataBaseBooks){

            mCategoryDao=dataBaseBooks.getCategoryDao();
            mBookDao=dataBaseBooks.getBookDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Category category1=new Category();
            category1.setName("Text Books");
            category1.setDescription("Text Books Description");

            Category category2=new Category();
            category2.setName("Novels");
            category2.setDescription("Novels Description");

            Category category3=new Category();
            category3.setName("Other Books");
            category3.setDescription("Text Books Description");

            mCategoryDao.insertCategory(category1);
            mCategoryDao.insertCategory(category2);
            mCategoryDao.insertCategory(category3);

            Book book1=new Book();
            book1.setBookName("High school Java ");
            book1.setUnitPrice("$150");
            book1.setCategoryId(1);

            Book book2=new Book();
            book2.setBookName("Mathematics for beginners");
            book2.setUnitPrice("$200");
            book2.setCategoryId(1);

            Book book3=new Book();
            book3.setBookName("Object Oriented Androd App Design");
            book3.setUnitPrice("$150");
            book3.setCategoryId(1);

            Book book4=new Book();
            book4.setBookName("Astrology for beginners");
            book4.setUnitPrice("$190");
            book4.setCategoryId(1);

            Book book5=new Book();
            book5.setBookName("High school Magic Tricks ");
            book5.setUnitPrice("$150");
            book5.setCategoryId(1);

            Book book6=new Book();
            book6.setBookName("Chemistry  for secondary school students");
            book6.setUnitPrice("$250");
            book6.setCategoryId(1);

            Book book7=new Book();
            book7.setBookName("A Game of Cats");
            book7.setUnitPrice("$19.99");
            book7.setCategoryId(2);

            Book book8=new Book();
            book8.setBookName("The Hound of the New York");
            book8.setUnitPrice("$16.99");
            book8.setCategoryId(2);

            Book book9=new Book();
            book9.setBookName("Adventures of Joe Finn");
            book9.setUnitPrice("$13");
            book9.setCategoryId(2);

            Book book10=new Book();
            book10.setBookName("Arc of witches");
            book10.setUnitPrice("$19.99");
            book10.setCategoryId(2);

            Book book11=new Book();
            book11.setBookName("Can I run");
            book11.setUnitPrice("$16.99");
            book11.setCategoryId(2);

            Book book12=new Book();
            book12.setBookName("Story of a joker");
            book12.setUnitPrice("$13");
            book12.setCategoryId(2);

            Book book13=new Book();
            book13.setBookName("Notes of a alien life cycle researcher");
            book13.setUnitPrice("$1250");
            book13.setCategoryId(3);

            Book book14=new Book();
            book14.setBookName("Top 9 myths abut UFOs");
            book14.setUnitPrice("$789");
            book14.setCategoryId(3);

            Book book15=new Book();
            book15.setBookName("How to become a millionaire in 24 hours");
            book15.setUnitPrice("$1250");
            book15.setCategoryId(3);

            Book book16=new Book();
            book16.setBookName("1 hour work month");
            book16.setUnitPrice("$199");
            book16.setCategoryId(3);

            mBookDao.insertBook(book1);
            mBookDao.insertBook(book2);
            mBookDao.insertBook(book3);
            mBookDao.insertBook(book4);
            mBookDao.insertBook(book5);
            mBookDao.insertBook(book6);
            mBookDao.insertBook(book7);
            mBookDao.insertBook(book8);
            mBookDao.insertBook(book9);
            mBookDao.insertBook(book10);
            mBookDao.insertBook(book11);
            mBookDao.insertBook(book12);
            mBookDao.insertBook(book13);
            mBookDao.insertBook(book14);
            mBookDao.insertBook(book15);
            mBookDao.insertBook(book16);
            return null;
        }
    }
}
