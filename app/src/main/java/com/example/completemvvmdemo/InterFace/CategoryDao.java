package com.example.completemvvmdemo.InterFace;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.completemvvmdemo.model.Category;

import java.util.List;

@Dao
public interface CategoryDao {
    @Insert
    public void insertCategory(Category category);

    @Update
    public void updateCategory(Category category);

    @Delete
    public void deleteCategory(Category category);

    @Query("select * from category_table")
    public LiveData<List<Category>> getAllCategory();

}
