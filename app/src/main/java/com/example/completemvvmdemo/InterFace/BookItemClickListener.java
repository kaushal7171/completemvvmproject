package com.example.completemvvmdemo.InterFace;

import com.example.completemvvmdemo.model.Book;

public interface BookItemClickListener {
    public void itemClick(Book book);
}
