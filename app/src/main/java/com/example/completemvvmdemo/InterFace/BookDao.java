package com.example.completemvvmdemo.InterFace;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.example.completemvvmdemo.model.Book;

import java.util.List;

@Dao
public interface BookDao {
    @Insert
    void insertBook(Book book);

    @Update
    void updateBook(Book book);

    @Delete
     void deleteBook(Book book);

    @Query("select * from book_table")
     LiveData<List<Book>> getAllBook();

    @Query("select * from book_table where Category_Id==:category_id")
     LiveData<List<Book>> getBooks(int category_id);
}
