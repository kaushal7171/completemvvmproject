package com.example.completemvvmdemo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.completemvvmdemo.InterFace.BookItemClickListener;
import com.example.completemvvmdemo.model.Book;
import com.example.completemvvmdemo.databinding.BookItemBinding;

import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder> {
    BookItemClickListener bookItemClickListener;
    ArrayList<Book> booksList;

    public void setBookItemClickListener(BookItemClickListener bookItemClickListener) {
        this.bookItemClickListener = bookItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BookItemBinding bookItemBinding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.book_item,parent,false);
        return new MyViewHolder(bookItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bookItemBinding.setBook(booksList.get(position));

    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }

    public void setBooksList(ArrayList<Book> newbooksList) {
        /*this.booksList = booksList;
        notifyDataSetChanged();*/

        final DiffUtil.DiffResult diffResult= DiffUtil.calculateDiff(new BookDefUtils(booksList,newbooksList));
        booksList=newbooksList;
        diffResult.dispatchUpdatesTo(BookAdapter.this);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private BookItemBinding bookItemBinding;

        public MyViewHolder(@NonNull BookItemBinding bookItemBinding) {
            super(bookItemBinding.getRoot());
            this.bookItemBinding=bookItemBinding;
            bookItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(bookItemClickListener!=null && getAdapterPosition()!=RecyclerView.NO_POSITION) {
                        bookItemClickListener.itemClick(booksList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }


}

