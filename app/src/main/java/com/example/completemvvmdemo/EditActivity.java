package com.example.completemvvmdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.completemvvmdemo.databinding.ActivityEditBinding;
import com.example.completemvvmdemo.model.Book;

public class EditActivity extends AppCompatActivity {
    Book book;
    ActivityEditBinding activityEditBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        book=new Book();
        activityEditBinding= DataBindingUtil.setContentView(this,R.layout.activity_edit);
        activityEditBinding.setBook(book);
        activityEditBinding.setClick(new EditActivityClickHandler(this));

        handleIntent();
    }

    private void handleIntent() {
        Book bookObj= (Book) getIntent().getSerializableExtra("bookObj");
        if(bookObj!=null) {
            book.setBookId(bookObj.getBookId());
            book.setBookName(bookObj.getBookName());
            book.setUnitPrice(bookObj.getUnitPrice());
            book.setCategoryId(bookObj.getCategoryId());
            activityEditBinding.update.setText("Update");
        }else {
            activityEditBinding.update.setText("Add");
        }
    }

    public class EditActivityClickHandler{
        private Context context;

        public EditActivityClickHandler(Context context) {
            this.context = context;
        }
        public void sumbitButtonClickEvent(View view){


            Intent intent=new Intent();
            intent.putExtra("EditBook",book);
            setResult(RESULT_OK,intent);
            finish();

        }
    }
}